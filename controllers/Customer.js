import { Customer } from "../models/Customer.js";


// hàm này thực hiện việc thêm mới người dùng
export const createCustomer = async (req, res) => {
  try {
    const newCustomer = req.body;
    const customer = Customer(newCustomer);
    await customer.save();
    res.status(200).json({
      success: true,
      message: "Customer created successfully",
      customer: customer,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "create failed",
      error: err.message,
    });
  }
};



// hàm này thực việc trả ra toàn bộ user trên database
export const getAllCustomer = async (req, res) => {
  try {
    const allCustomer = await Customer.find();
    res.status(200).json({
      success: true,
      message: "all customer",
      Customer: allCustomer,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "Customer not found",
      error: err.message,
    });
  }
};


// hàm này thực hiện việc trả ra user dựa vào id
export const getCustomerByCustomerId = async (req, res) => {
  try {
    const customerId = req.params.customerid;
    const customer = await Customer.findOne({ _id: customerId });
    res.status(200).json({
      success: true,
      message: "Customer exists",
      Customer: customer,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "Customer not found",
      error: err.message,
    });
  }
};



// hàm này trả ra user dựa vào email
export const getCustomerByEmail = async (req, res) => {
  try {
    const email = req.query.email;
    const customer = await Customer.findOne({ email: email });
    res.status(200).json({
      success: true,
      message: "Customer exists",
      customer: customer,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "Customer not found",
      error: err.message,
    });
  }
};


// hàm này thực hiện xóa user dựa vào email
export const deleteCustomerByEmail = async (req, res) => {
  try {
    const email = req.query.email;
    const customer = await Customer.findOneAndDelete({ email: email });
    res.status(200).json({
      success: true,
      message: "Customer deleted",
      Customer: customer,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "Customer not found",
      error: err.message,
    });
  }
};


// hàm này thực hiện lấy ra user dựa vào số điện thoại
export const getCustomerByPhoneNumber = async (req, res) => {
  try {
    const phoneNumber = req.query.phone;
    const customer = await Customer.findOne({ phone: phoneNumber });
    res.status(200).json({
      success: true,
      message: "Customer exists",
      customer: customer,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "search failed",
      error: err.message,
    });
  }
};


// hàm này thực hiện việc cho user đăng nhập dựa vào số điện thoại hoặc email
export const loginCustomerEmailOrPhone = async (req, res) => {
  try {
    const login = req.query.login;
    const customer = await Customer.findOne({
      $or: [{ email: login }, { phone: login }],
    });
    res.status(200).json({
      success: true,
      message: "Success",
      customer: customer,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "login failed",
      error: err.message,
    });
  }
};


// hàm này thực hiện việc cho user update theo email hoặc số điện thoại
export const updateCustomerByEmailAndPhone = async (req, res) => {
  try {
    const email = req.params.email;
    const phone = req.params.phone;
    const newCus = req.body;
    const customer = await Customer.findOneAndUpdate(
      { $and: [{ email: email }, { phone: phone }] },
      newCus,
      {
        new: true,
      }
    );
    res.status(200).json({
      success: true,
      message: email,
      customer: customer,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "update failed",
      error: err.message,
    });
  }
};


// hàm này thực hiện việc update user dựa vào số điện thoại
export const updateCustomerByPhone = async (req, res) => {
  try {
    const phone = req.query.phone;
    const updateCus = req.body;
    const customer = await Customer.findOneAndUpdate(
      { phone: phone },
      updateCus,
      { new: true }
    );
    res.status(200).json({
      success: true,
      message: "update",
      customer: customer,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "update failed",
      error: err.message,
    });
  }
};

// hàm này thực hiện update user dựa vào email
export const updateCustomerByEmail = async (req, res) => {
  try {
    const email = req.query.email;
    const updateCus = req.body;
    const customer = await Customer.findOneAndUpdate(
      { email: email },
      updateCus,
      { new: true }
    );
    res.status(200).json({
      success: true,
      message: "update",
      customer: customer,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "update failed",
      error: err.message,
    });
  }
};


// hàm này thực hiện lấy ra user dựa vào email và số điện thoại
export const getCustomerByEmailAndPhone = async (req, res) => {
  try {
    const email = req.query.email;
    const phone = req.query.phone;
    const customer = await Customer.findOne({
      $or: [{ email: email }, { phone: phone }],
    });
    res.status(200).json({
      success: true,
      message: "get successful",
      customer: customer,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "get failed",
      error: err.message,
    });
  }
};


// hàm này thực hiện việc update user dựa vào email hoặc số điện thoại
export const updateCustomerByEmailOrPhone = async (req, res) => {
  try {
    const email = req.query.email;
    const updateCus = req.body;
    const customer = await Customer.findOneAndReplace(
      { email: email },
      updateCus,
      { new: true }
    );
    res.status(200).json({
      success: true,
      message: "Customer updated successfully",
      customer: customer,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "update failed",
      error: err.message,
    });
  }
};


// hàm thực hiện update user theo id
export const updateCustomerById = async (req, res) => {
  try {
    const userId = req.query.id;
    const newUser = req.body;
    const customer = await Customer.findOneAndUpdate({ _id: userId }, newUser, {
      new: true,
    });
    res.status(200).json({
      success: true,
      message: "update successful",
      customer: customer,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "update failed",
      error: err.message,
    });
  }
};

// hàm này thực hiện lấy ra user dựa vào email hoặc số điện thoại
export const getCustomerByEmailOrPhone = async (req, res) => {
  try {
    const key = req.query.key;
    const customer = await Customer.findOne({
      $or: [{ email: String(key) }, { phone: String(key) }],
    });
    res.status(200).json({
      success: true,
      message: "find exists",
      customer: customer,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "search failed",
      error: err.message,
    });
  }
};
