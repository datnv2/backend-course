import { Order } from "../models/Order.js";

export const createOrder = async (req, res) => {
  try {
    const order = new Order({
      customerId: req.body.customerId,
      orderDate: req.body.orderDate,
      requiredDate: req.body.requiredDate,
      shippedDate: req.body.shippedDate,
      note: req.body.note,
      status: req.body.status,
      timeCreate: req.body.timeCreate,
      timeUpdate: req.body.timeUpdate,
    });
    await order.save();
    res.status(200).json({
      success: true,
      message: "Order created successfully",
      Order: order,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "create failed",
      error: err.message,
    });
  }
};
export const getAllOrder = async (req, res) => {
  try {
    const order = await Order.find();
    res.status(200).json({
      success: true,
      message: "Order exists",
      Order: order,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "Order not found",
      error: err.message,
    });
  }
};
export const getOrderByCustomerId = async (req, res) => {
  try {
    const customerId = req.params.customerid;
    const order = await Order.find({
      customerId: customerId,
    });
    res.status(200).json({
      success: true,
      message: "Order exists",
      Order: order,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "Order not found",
      error: err.message,
    });
  }
};
export const updateOrderByOrderId = async (req, res) => {
  try {
    const orderId = req.params.orderid;
    const newOrder = req.body;
    const order = await Order.findOneAndUpdate({ _id: orderId }, newOrder, {
      new: true,
    });
    res.status(200).json({
      success: true,
      message: "Order updated",
      Order: order,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "Order update failed",
      error: err.message,
    });
  }
};
export const deleteOrderByOrderId = async (req, res) => {
  try {
    const orderid = req.params.orderid;
    const order = await Order.findOneAndDelete({ _id: orderid });
    res.status(200).json({
      success: true,
      message: "Order deleted successfully",
      Order: order,
    });
  } catch (err) {
    res.status.json({
      success: false,
      message: "delete failed",
      error: err.message,
    });
  }
};

export const getOrderByOrderId = async (req, res) => {
  try {
    const orderId = req.params.orderid;
    const order = await Order.findOne({ _id: orderId });
    res.status(200).json({
      success: true,
      message: "Order found successfully",
      order: order,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "Error getting order",
      error: err.message,
    });
  }
};
