import { OrderDetail } from "../models/OrderDetail.js";

// hàm này để thực hiện thêm order detail
export const createOrderDetail = async (req, res) => {
  try {
    const orderDetail = new OrderDetail({
      orderId: req.body.orderId,
      productId: req.body.productId,
      quantity: req.body.quantity,
      priceEach: req.body.priceEach,
    });
    await orderDetail.save();
    res.status(200).json({
      success: true,
      message: "OrderDetail created",
      OrderDetail: orderDetail,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "create failed",
      error: err.message,
    });
  }
};

// hàm này để lấy ra tất cả order detail
export const getAllOrderDetail = async (req, res) => {
  try {
    const allOrderDetail = await OrderDetail.find();
    res.status(200).json({
      success: true,
      message: "OrderDetail exists",
      OrderDetail: allOrderDetail,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "Order detail not found",
      error: err.message,
    });
  }
};

// hàm này để lấy ra order detail dựa vào detail id
export const getOrderDetailByOrderDetailId = async (req, res) => {
  try {
    const orderDtailId = req.params.orderdetailid;
    const orderDetail = await OrderDetail.findOne({ _id: orderDtailId });
    res.status(200).json({
      success: true,
      message: "OrderDetail exists",
      OrderDetail: orderDetail,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "OrderDetail not found",
      error: err.message,
    });
  }
};

// hàm này thực hiện việc update order detail
export const updateOrderDetailByOrderDetailId = async (req, res) => {
  try {
    const orderDetailId = req.params.orderdetailid;
    const newOrderDetail = req.body;
    const orderDetail = await OrderDetail.findOneAndUpdate(
      {
        _id: orderDetailId,
      },
      newOrderDetail,
      { new: true }
    );
    res.status(200).json({
      success: true,
      message: "OrderDetail updated",
      OrderDetail: orderDetail,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "OrderDetail update failed",
      error: err.message,
    });
  }
};

// hàm này thực hiện việc xóa order detail
export const deleteOrderDetail = async (req, res) => {
  try {
    const orderDetailId = req.params.orderdetailid;
    const orderDetail = await OrderDetail.findOneAndDelete({
      _id: orderDetailId,
    });
    res.status(200).json({
      success: true,
      message: "OrderDetail deleted",
      OrderDetail: orderDetail,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "OrderDetail delete failed",
      error: err.message,
    });
  }
};

// hàm này thực hiện việc lấy ra order detail dựa vào order id
export const getOrderDetailByOrderId = async (req, res) => {
  try {
    const orderId = req.params.order;
    const orderDetail = await OrderDetail.findOne({ orderId: orderId });
    res.status(200).json({
      success: true,
      message: "OrderDetail exists",
      orderDetail: orderDetail,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "OrderDetail get failed",
      error: err.message,
    });
  }
};

export const getOrderDetailById = async (req, res) => {
  try {
    const orderId = req.params.orderid;
    const orderDetail = await OrderDetail.find({ orderId: orderId });
    res.status(200).json({
      success: true,
      message: "OrderDetail exists",
      orderDetail: orderDetail,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "OrderDetail get failed",
      error: err.message,
    });
  }
};
