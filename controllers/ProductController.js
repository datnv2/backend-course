import { Product } from "../models/Product.js";


// hàm này thực hiện thêm mới sản phẩm 
export const createProduct = async (req, res) => {
  try {
    const newProduct = req.body;
    const product = Product(newProduct);
    await product.save();
    res.status(200).json({
      success: true,
      message: "create successfully",
      Product: product,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "create failed",
      error: err.message,
    });
  }
};

// hàm này thực hiện trả ra toàn bộ sản phẩm
export const getAllProduct = async (req, res) => {
  try {
    const allProduct = await Product.find();
    res.status(200).json({
      success: true,
      message: "get all",
      Product: allProduct,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "get all failed",
      error: err.message,
    });
  }
};

// hàm này thực hiện phân trang số lượng sản phẩm
export const getProductPage = async (req, res) => {
  const page = parseInt(req.query.page);
  const limit = 8;
  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;
  const results = {};
  if (endIndex < (await Product.countDocuments().exec())) {
    results.next = {
      page: page + 1,
      limit: limit,
    };
  }
  if (startIndex > 0) {
    results.previous = {
      page: page - 1,
      limit: limit,
    };
  }
  try {
    const allProduct = await Product.find();
    const pageNumber = allProduct.length / 8;
    const productList = await Product.find().limit(limit).skip(startIndex);
    res.status(200).json({
      success: true,
      message: "product list",
      pageNum: pageNumber,
      Product: productList,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "get all failed",
      error: err.message,
    });
  }
};


// hàm thực hiện trả ra sản phẩm theo id
export const getProductByProductId = async (req, res) => {
  try {
    const productId = req.params.productid;
    const product = await Product.findOne({ _id: productId });
    res.status(200).json({
      success: true,
      message: "Product is exists",
      Product: product,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "get product failed",
      error: err.message,
    });
  }
};

// hàm này thực hiện việc update sản phẩm theo id
export const updateProductByProductId = async (req, res) => {
  try {
    const productId = req.params.productid;
    const newProduct = req.body;
    const product = await Product.findOneAndUpdate(
      { _id: productId },
      newProduct
    );
    res.status(200).json({
      success: true,
      message: "Product updated successfully",
      Product: newProduct,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "update product failed",
      error: err.message,
    });
  }
};

// hàm này thực hiện xóa sản phẩm theo id
export const deleteProductByProductId = async (req, res) => {
  try {
    const productId = req.params.productid;
    const product = await Product.findOneAndDelete({ _id: productId });
    res.status(200).json({
      success: true,
      message: "Product deleted successfully",
      Product: product,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "delete product failed",
      error: err.message,
    });
  }
};


// hàm này thực hiện việc lấy ra sản phẩm theo giá thấp nhất
export const getProductByPriceMin = async (req, res) => {
  try {
    const page = parseInt(req.query.page);
    const limit = 8;
    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;
    const results = {};
    if (endIndex < (await Product.countDocuments().exec())) {
      results.next = {
        page: page + 1,
        limit: limit,
      };
    }
    if (startIndex > 0) {
      results.previous = {
        page: page - 1,
        limit: limit,
      };
    }
    const priceMin = parseInt(req.query.pricemin);
    const listProduct = await Product.find({
      productPrice: { $gte: priceMin },
    })
      .limit(limit)
      .skip(startIndex);
    res.status(200).json({
      success: true,
      message: "product list by product price min",
      pageNum: listProduct.length / 8,
      Product: listProduct,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "get product failed",
      error: err.message,
    });
  }
};

// hàm trả ra các sản phẩm ở trang carosoule
export const getProductCarousel = async (req, res) => {
  try {
    const productList = await Product.find().limit(4);
    res.status(200).json({
      success: true,
      message: "product list by product",
      Product: productList,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "get product failed",
      error: err.message,
    });
  }
};


// hàm này thực hiện việc trả ra sản phẩm trên mỗi trang
export const getProductLast = async (req, res) => {
  try {
    const lastProduct = await Product.find().limit(8);
    res.status(200).json({
      success: true,
      message: "product last by product",
      Product: lastProduct,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "get failed",
      error: err.message,
    });
  }
};

// hàm này thực hiện tìm kiếm sản phẩm theo tên
export const getProductByKey = async (req, res) => {
  try {
    const keyWord = req.query.keyword;
    const field = req.query.field;
    let product = "";
    if (field === "all") {
      product = await Product.find({
        productName: keyWord,
      });
      res.status(200).json({
        success: true,
        message: "search exists",
        product: product,
      });
      return;
    }
    if (field === "name") {
      product = await Product.find({
        productName: keyWord,
      });
      res.status(200).json({
        success: true,
        message: "search exists",
        product: product,
      });
      return;
    }
    if (field === "type") {
      product = await Product.find({
        $and: [{ productType: field }, { productType: keyWord }],
      });
      res.status(200).json({
        success: true,
        message: "search exists",
        product: product,
      });
      return;
    }
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "get failed",
      error: err.message,
    });
  }
};
