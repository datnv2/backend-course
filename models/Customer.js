import mongoose from "mongoose";

const { Schema } = mongoose;

const customer = new Schema({
  fullName: { type: String },
  phone: { type: String },
  email: { type: String },
  photoURL: { type: String },
  address: { type: String },
  timeCreate: { type: Date, default: Date.now },
  timeUpdate: { type: Date, default: Date.now },
});

export const Customer = mongoose.model("Customer", customer);
