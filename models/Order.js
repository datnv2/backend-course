import mongoose from "mongoose";
const { Schema } = mongoose;

const order = new Schema({
  customerId: { type: Schema.Types.ObjectId },
  orderDate: { type: Date, default: Date.now },
  requiredDate: { type: Date, default: Date.now },
  shippedDate: { type: Date, default: Date.now },
  note: { type: String, default: null },
  status: { type: Number, default: 0 },
  timeCreate: { type: Date, default: Date.now },
  timeUpdate: { type: Date, default: Date.now },
});
export const Order = mongoose.model("Order", order);
