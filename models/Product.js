import mongoose from "mongoose";

const { Schema } = mongoose;

const product = new Schema({
  productName: { type: String },
  productType: { type: String },
  productImage: [String],
  productQuantity: { type: Number, default: 1 },
  productPrice: { type: Number },
  priceSale: { type: Number },
  studyTime: { type: String },
  level: { type: String },
  productDetail: { type: String },
  teacherAvarta: [String],
  teacherName: { type: String },
  timeCreate: { type: Date, default: Date.now },
  timeUpdate: { type: Date, default: Date.now },
});
export const Product = mongoose.model("Product", product);
