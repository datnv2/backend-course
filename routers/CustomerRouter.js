import express from "express";
import {
  createCustomer,
  getAllCustomer,
  getCustomerByCustomerId,
  getCustomerByEmail,
  deleteCustomerByEmail,
  getCustomerByPhoneNumber,
  loginCustomerEmailOrPhone,
  updateCustomerByEmailAndPhone,
  updateCustomerByEmail,
  updateCustomerByPhone,
  getCustomerByEmailAndPhone,
  updateCustomerByEmailOrPhone,
  updateCustomerById,
  getCustomerByEmailOrPhone,
} from "../controllers/Customer.js";
const router = express.Router();

router.get("/customer", getAllCustomer);

router.get("/customer/:customerid", getCustomerByCustomerId);

router.post("/customer", createCustomer);

router.get("/customer-email", getCustomerByEmail);

router.delete("/customer", deleteCustomerByEmail);

router.get("/customer-phone", getCustomerByPhoneNumber);

router.get("/customer-login", loginCustomerEmailOrPhone);

router.put("/customer/update/:email/:phone", updateCustomerByEmailAndPhone);

router.put("/customer/update-email", updateCustomerByEmail);

router.put("/customer/update-phone", updateCustomerByPhone);

router.get("/customer-email-phone", getCustomerByEmailAndPhone);

router.put("/customer-update-email-or-phone", updateCustomerByEmailOrPhone);

router.put("/customer-update", updateCustomerById);

router.get("/customer-by", getCustomerByEmailOrPhone);

export default router;
