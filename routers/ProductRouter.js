import express from "express";
import {
  createProduct,
  getProductPage,
  getAllProduct,
  getProductByProductId,
  updateProductByProductId,
  deleteProductByProductId,
  getProductByPriceMin,
  getProductCarousel,
  getProductLast,
  getProductByKey,
} from "../controllers/ProductController.js";
const router = express.Router();

router.post("/products", createProduct);
router.get("/products", getAllProduct);
router.get("/products/:productid", getProductByProductId);
router.put("/products/:productid", updateProductByProductId);
router.delete("/products/:productid", deleteProductByProductId);
router.get("/all-products", getProductPage);
router.get("/filter-products", getProductByPriceMin);
router.get("/product-carousel", getProductCarousel);
router.get("/products-last", getProductLast);
router.get("/products-search", getProductByKey);
export default router;
